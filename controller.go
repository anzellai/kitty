package kitty

import (
	"encoding/json"
	"errors"
	"net/http"
)

// APICreate will parse request body into dst
func APICreate(w http.ResponseWriter, r *http.Request, ds Datastore, dst interface{}) (err error) {
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	err = decoder.Decode(&dst)
	if err != nil {
		err = errors.New("cannot parse JSON request body")
	}

	if ds.GetID() != "" {
		return errors.New("cannot use pre-allocated uid")
	}

	ctx := AppContext(r)
	err = Save(ctx, ds)
	return
}
