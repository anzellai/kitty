package kitty

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"google.golang.org/appengine/log"
)

const (
	// MaxBytesInRequestBody limits incoming request body size to avoid malicious attack
	MaxBytesInRequestBody int64 = 1024 * 10
)

// MiddlewareAPIContentType limits requests with only json content-type
func MiddlewareAPIContentType(next http.Handler) http.Handler {
	accepted := GetEnv("API_ACCEPTED_CONTENT_TYPE")
	allowedOrigins := GetEnv("API_ALLOWED_ORIGINS")
	siteName := strings.Join(strings.Split(GetEnv("SITE_NAME"), " "), "-")
	if siteName == "" {
		siteName = "Project"
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		contentType := r.Header.Get("Content-Type")
		contentOrigin := fmt.Sprintf("%s,", r.Header.Get(fmt.Sprintf("%s-Origin", siteName)))
		if contentOrigin == "" || !strings.Contains(allowedOrigins, contentOrigin) {
			RenderJSON(w, r, http.StatusNotAcceptable, errors.New("not acceptable content origin"), nil)
			return
		}
		if accepted == "" || strings.HasPrefix(contentType, accepted) {
			next.ServeHTTP(w, r)
			return
		}
		RenderJSON(w, r, http.StatusNotAcceptable, errors.New("not acceptable Content-Type"), nil)
	})
}

func getMaxBytesAllowedInRequestBody(ctx context.Context) int64 {
	maxBytesAllowedString := GetEnv("MAX_BYTES_ALLOWED_IN_REQUEST_BODY")
	if maxBytesAllowedString == "" {
		return MaxBytesInRequestBody
	}
	maxBytesAllowed, err := strconv.ParseInt(maxBytesAllowedString, 10, 64)
	if err != nil {
		log.Warningf(ctx, "invalid max bytes allowed in request body: %+v", err)
		return MaxBytesInRequestBody
	}
	return maxBytesAllowed
}

// MiddlewareMaxRequestBody limits request body to max size
func MiddlewareMaxRequestBody(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := AppContext(r)
		r.Body = http.MaxBytesReader(w, r.Body, getMaxBytesAllowedInRequestBody(ctx))
		next.ServeHTTP(w, r)
	})
}

// MiddlewareRecoverPanic middleware recovers any panic and email Admin for errors
func MiddlewareRecoverPanic(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error
		ctx := AppContext(r)

		defer func() {
			recovered := recover()
			if recovered != nil {
				switch t := recovered.(type) {
				case string:
					err = errors.New(t)
				case error:
					err = t
				default:
					err = errors.New("unknown error causing panic")
				}

				errorMessage := fmt.Sprintf("Server panic recovered with error: %+v", err)

				log.Criticalf(ctx, errorMessage)
				NotifyAdmin(ctx, errorMessage, &BaseModel{}, err)

				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		}()
		h.ServeHTTP(w, r)
	})
}
