// Copyright © 2018 Anzel Lai <anzel_lai@hotmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"

	"bitbucket.org/anzellai/kitty"
	"github.com/spf13/cobra"
)

var (
	destPath, packageName   string
	modelPath, modelPackage string
)

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate [--DEST] [--PACKAGE] [--SOURCE] [--MODEL PACKAGE] [ENTITY...]",
	Short: "generate auto rest api and router hook",
	Long: `generate auto rest api and router hook

Please note SOURCE and DEST must reside under "vendor" folder
	`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			fmt.Println("at least 1 Entity name is required")
			os.Exit(1)
		}
		entities := kitty.NewEntity()
		for _, name := range args {
			entities.Register(name)
		}

		err := entities.GenerateAPI(destPath, packageName, modelPath, modelPackage)
		if err != nil {
			fmt.Println("error executing generate command: ", err.Error())
			os.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)

	generateCmd.Flags().StringVarP(&destPath, "dest", "d", ".", "destination path for generated package")
	generateCmd.Flags().StringVarP(&packageName, "package", "p", "", "name for generated package")
	generateCmd.Flags().StringVarP(&modelPath, "src", "s", "", "source path of model schema for generated model (required)")
	generateCmd.Flags().StringVarP(&modelPackage, "model", "m", "", "package name of source for generated model (required")

	generateCmd.MarkFlagRequired("src")
	generateCmd.MarkFlagRequired("model")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
