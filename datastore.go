package kitty

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	valid "github.com/asaskevich/govalidator"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/appengine/datastore"
)

// BaseModel is a base struct for embedding fields and methods
type BaseModel struct {
	UID     string    `valid:",optional,uuidv4"`
	Created time.Time `valid:"-"`
	Updated time.Time `valid:"-"`
}

// GetID for BaseModel
func (ds *BaseModel) GetID() string {
	return ds.UID
}

// SetID for BaseModel
func (ds *BaseModel) SetID(uid string) {
	ds.UID = uid
}

// Validate for BaseModel (to override)
func (ds *BaseModel) Validate() error {
	return ValidateSchema(ds)
}

// BeforeCreate for BaseModel (to override)
func (ds *BaseModel) BeforeCreate(context.Context) error {
	return nil
}

// BeforeSave for BaseModel (to override)
func (ds *BaseModel) BeforeSave(context.Context) error {
	now := time.Now()
	if ds.Created.IsZero() {
		ds.Created = now
	}
	ds.Updated = now

	return nil
}

// AfterSave for BaseModel (to override)
func (ds *BaseModel) AfterSave(context.Context) {

}

// String for BaseModel (to override)
func (ds *BaseModel) String() string {
	return "BaseModel"
}

// Summary for BaseModel (to override)
func (ds *BaseModel) Summary() string {
	return fmt.Sprintf("<%s : %s>", ds.String(), ds.GetID())
}

// Filter ...
type Filter struct {
	Key   string
	Value interface{}
}

// Datastore is an interface for appengine datastore to populate key and save
type Datastore interface {
	GetID() string
	SetID(string)
	BeforeCreate(context.Context) error
	BeforeSave(context.Context) error
	AfterSave(context.Context)
	Validate() error
	String() string
	Summary() string
}

// ValidateSchema for Datastore
func ValidateSchema(ds Datastore) error {
	_, err := valid.ValidateStruct(ds)
	return err
}

// Key will get or generate and return a datastore Key type
func Key(ctx context.Context, ds Datastore) (*datastore.Key, error) {
	// if there is no ID, we want to generate an "incomplete"
	// one and let datastore determine the key/Id for us
	if ds.GetID() == "" {
		uid := uuid.NewV4()
		ds.SetID(uid.String())
	}

	// if ID is already set, we will just build the Key based
	// on the one provided.
	return datastore.NewKey(ctx, ds.String(), ds.GetID(), 0, nil), nil
}

// Save will save the Datastore type struct in datastore
func Save(ctx context.Context, ds Datastore) error {
	if ds.GetID() == "" {
		err := ds.BeforeCreate(ctx)
		if err != nil {
			return err
		}
	}
	// reference the key function and generate it
	// accordingly basically its isNew true/false
	key, err := Key(ctx, ds)
	if err != nil {
		return err
	}

	// validate datastore
	err = ds.Validate()
	if err != nil {
		return err
	}

	// run BeforeSave hook
	err = ds.BeforeSave(ctx)
	if err != nil {
		return nil
	}

	_, err = datastore.Put(ctx, key, ds)
	ds.AfterSave(ctx)

	return err
}

// Delete will delete the record in DB
func Delete(ctx context.Context, ds Datastore) error {
	if ds.GetID() == "" {
		return errors.New("cannot delete with empty uid")
	}

	key, err := Key(ctx, ds)
	if err != nil {
		return err
	}

	return datastore.Delete(ctx, key)
}

// KeyWithParent will get or generate and return a datastore Key type with Parent entity
func KeyWithParent(ctx context.Context, ds, parent Datastore) (*datastore.Key, error) {
	if parent.GetID() == "" {
		return nil, errors.New("cannot use parent with empty uid")
	}

	if ds.GetID() == "" {
		uid := uuid.NewV4()
		ds.SetID(uid.String())
	}

	parentKey, err := Key(ctx, parent)
	if err != nil {
		return nil, err
	}
	return datastore.NewKey(ctx, ds.String(), ds.GetID(), 0, parentKey), nil
}

// SaveWithParent will save the Datastore type struct with Parent entity in datastore
func SaveWithParent(ctx context.Context, ds Datastore, parent Datastore) error {
	key, err := KeyWithParent(ctx, ds, parent)
	if err != nil {
		return err
	}

	// validate datastore
	err = ds.Validate()
	if err != nil {
		return err
	}

	// run BeforeSave hook
	err = ds.BeforeSave(ctx)
	if err != nil {
		return nil
	}

	_, err = datastore.Put(ctx, key, ds)
	ds.AfterSave(ctx)

	return err
}

// DeleteWithParent will delete the record with provided Parentin DB
func DeleteWithParent(ctx context.Context, ds Datastore, parent Datastore) error {
	key, err := KeyWithParent(ctx, ds, parent)
	if err != nil {
		return err
	}

	return datastore.Delete(ctx, key)
}

// FieldNames returns a slice of exported datastore field names
func FieldNames(ds interface{}) []string {
	output := StructToMap(ds)
	return MapKeys(output)
}

// Query will instantiate a new datastore query for given entity
func Query(ds Datastore) *datastore.Query {
	return datastore.NewQuery(ds.String())
}

// QueryPager will instantiate query with paging parameters
func QueryPager(
	query *datastore.Query,
	offset, limit int,
	orderBy string,
) *datastore.Query {
	if orderBy == "" {
		orderBy = "-Created"
	}
	query = query.Order(orderBy)
	if offset > 0 {
		query = query.Offset(offset)
	}
	if limit > 0 {
		query = query.Limit(limit)
	}

	return query
}

// QueryFilter will instantiate query with filters
func QueryFilter(
	query *datastore.Query,
	filtersBy ...Filter,
) (*datastore.Query, error) {
	if len(filtersBy) == 0 {
		return nil, errors.New("missing filter/value pairs")
	}
	for _, filter := range filtersBy {
		query = query.Filter(filter.Key, filter.Value)
	}

	return query, nil
}

// FindAll will parse dst from datastore with paging parameters
func FindAll(
	ctx context.Context,
	ds Datastore,
	dst interface{},
	offset, limit int,
	orderBy string,
) (err error) {
	query := Query(ds)
	query = QueryPager(query, offset, limit, orderBy)

	_, err = query.GetAll(ctx, dst)
	if _, ok := err.(*datastore.ErrFieldMismatch); ok {
		err = nil
	}
	return
}

// FindOne will parse dst from datastore with uid of ds
func FindOne(
	ctx context.Context,
	ds Datastore,
	dst interface{},
) (err error) {
	if ds.GetID() == "" {
		return errors.New("cannot query with empty uid")
	}

	key, err := Key(ctx, ds)
	if err != nil {
		return
	}

	err = datastore.Get(ctx, key, dst)
	if _, ok := err.(*datastore.ErrFieldMismatch); ok {
		err = nil
	}
	return
}

// FilterBy will parse dst from datastore with fitlers and paging parameters
func FilterBy(
	ctx context.Context,
	ds Datastore,
	dst interface{},
	offset, limit int,
	orderBy string,
	filtersBy ...Filter,
) (err error) {
	query := Query(ds)
	query, err = QueryFilter(query, filtersBy...)
	if err != nil {
		return
	}
	query = QueryPager(query, offset, limit, orderBy)

	_, err = query.GetAll(ctx, dst)
	if _, ok := err.(*datastore.ErrFieldMismatch); ok {
		err = nil
	}

	return
}

// RequestFilter parse querystring from request and return as filters
func RequestFilter(r *http.Request, ds Datastore) (
	offset, limit int,
	orderBy string,
	filtersBy []Filter,
) {
	qs := r.URL.Query()
	var err error

	offset, err = strconv.Atoi(qs.Get("offset"))
	if err != nil {
		offset = 0
	}
	limit, err = strconv.Atoi(qs.Get("limit"))
	if err != nil {
		limit = 0
	}
	orderBy = qs.Get("orderBy")

	fieldNames := FieldNames(ds)

	for fieldName := range qs {
		if fieldName == "" {
			continue
		}

		fieldValue := qs.Get(fieldName)
		op := " ="

		ops := strings.Split(fieldName, "__")
		if len(ops) == 2 {
			if ops[1] == "gt" {
				op = " >"
				fieldName = ops[0]
			} else if ops[1] == "lt" {
				op = " <"
				fieldName = ops[0]
			} else if ops[1] == "gte" {
				op = " >="
				fieldName = ops[0]
			} else if ops[1] == "lte" {
				op = " <="
				fieldName = ops[0]
			}
		}

		if !HasStringInSlice(fieldNames, fieldName) {
			continue
		}

		if fieldValue != "" {
			filterBy := &Filter{
				Key:   fieldName + op,
				Value: fieldValue,
			}

			switch fieldValue {
			case "true":
				filterBy.Value = true
			case "false":
				filterBy.Value = false
			default:
				if strings.ContainsAny(fieldValue, ".") {
					floatValue, err := strconv.ParseFloat(fieldValue, 32)
					if err == nil {
						filterBy.Value = floatValue
					}
				} else {
					intValue, err := strconv.ParseInt(fieldValue, 10, 32)
					if err == nil {
						filterBy.Value = intValue
					}
				}
			}

			filtersBy = append(filtersBy, *filterBy)
		}
	}

	return
}
